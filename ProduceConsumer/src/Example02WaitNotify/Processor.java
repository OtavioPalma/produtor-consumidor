package Example02WaitNotify;

import static java.lang.Thread.sleep;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author avilapm
 */
public class Processor {

    private final Object lock = new Object(); //monitor

    public void produce() {

        synchronized (lock) {
            try {
                System.out.println(Thread.currentThread().getName() + " produtor executando e chamou wait...");
                lock.wait();
                System.out.println(Thread.currentThread().getName() + " produtor após wait...");
            } catch (InterruptedException ex) {
                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void consume() {

        Scanner scanner = new Scanner(System.in);
        synchronized (lock) {
           System.out.println("Consume executando e aguardando apertar uma tecla...");
            scanner.nextLine();
            System.out.println("Tecla pressionada...");
            lock.notify();
            
            /* try {
                sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
            } */

        }
    }
}

