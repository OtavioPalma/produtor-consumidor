package Example03ProducerConsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Rodar 
{
    Processor p1 = new Processor();
    ExecutorService threadExecutor = Executors.newFixedThreadPool(2);
        
    public void start()
    {
        threadExecutor.execute(() -> {
            p1.consume();
        });

        threadExecutor.execute(() -> {
            p1.produce();
        });

        threadExecutor.shutdown();
    }
    
    public void stop()
    {
        p1.setAux(false);
    }
}
