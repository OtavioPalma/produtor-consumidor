package Example03ProducerConsumer;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author avilapm
 */
public class Processor
{
    private final LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT = 100;
    private final Object lock = new Object(); //monitor
    private boolean aux = true;
    private JProgressBar barra;
    
    public void setBarra(JProgressBar barra)
    {
        this.barra = barra;
    }
    
    public void setAux(boolean aux)
    {
        this.aux = aux;
    }
    
    public void produce() 
    {
        int value = 0;
        while (aux) 
        {
            synchronized (lock) 
            {
                while (list.size() == LIMIT) 
                {
                    try 
                    {
                        lock.wait();
                    } 
                        catch (InterruptedException ex)
                        {
                            Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                }
                list.add(value++);
                lock.notify();
                barra.setValue(list.size());
                System.out.println(Thread.currentThread().getName() + " Produziu o valor : " + value);
            }
        }
    }

    public void consume()
    {
        while (aux)
        {
            synchronized (lock) 
            {
                System.out.print("Tamanho da lista eh: " + list.size());
                while (list.isEmpty()) 
                {
                    try 
                    {
                        lock.wait();
                    } 
                        catch (InterruptedException ex)
                        {
                            Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                }
                int value = list.removeFirst();
                barra.setValue(list.size());
                System.out.println(" ; Consumido valor eh: " + value);
                lock.notify();
            }
        }
    }
}
