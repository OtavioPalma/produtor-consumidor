package Example06ProducerConsumerGUI;

import java.util.LinkedList;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author avilapm
 */
public class Processor extends Observable {

    private AtomicBoolean bExecuta;
    private final LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT;
    private final Object lock; //monitor
    private String message;
    
    public Processor(){
        lock = new Object();
        bExecuta = new AtomicBoolean(true);
        LIMIT = 1000;
        message = "";
    }

    public void produce() {
        int value = 0;
        while (bExecuta.get()) {
            synchronized (lock) {

                while (list.size() == LIMIT) {
                    try {
                        lock.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                message = Thread.currentThread().getName() + " produziu o valor : " + value + "\n";
                setChanged();
                notifyObservers();
                list.add(value++);
                lock.notify();
            }
        }
    }

    public void consume() {
        while (bExecuta.get()) {
            synchronized (lock) {
                while (list.isEmpty()) {
                    try {
                        lock.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                int value = list.removeFirst();

                message = Thread.currentThread().getName() + " consumiu o valor : " + value + "\n";
                setChanged();
                notifyObservers();
                lock.notify();
            }
        }
    }

    public int getListSize() {
        return list.size();
    }

    public String getMessage() {
        return message;
    }
    
    public void setAtomicBoolean(boolean bExecuta){
        this.bExecuta.set(bExecuta);
    }
}
